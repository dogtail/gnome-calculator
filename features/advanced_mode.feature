@advanced_mode_feature
Feature: Advanced mode

  Background:
    * Set the "number-format" to "fixed" value
    * Set the "accuracy" to "9" value
    * Set the "show-thousands" to "false" value
    * Set the "angle-units" to "radians" value
    * Start application "gnome-calculator" via command "gnome-calculator -m advanced" in "session"
    * Wait until "Menu" "toggle button" appears


  @advanced_calculation
  Scenario Outline: Arithmentics
    * Calculate "<expression>"
    Then result is "<result>"

  Examples:
    | expression            | result     |
    | 123456789 + 987654321 | 1111111110 |
    | 987654321 + 0         | 987654321  |
    | 987654321 - 987654322 | -1         |
    | -98765-0              | -98765     |
    | 3/6                   | 0.5        |
    | -8/2                  | -4         |
    | 10 / 3 * 3            | 10         |
    | 6 / (3 * 2)           | 1          |


  @trigonometric_and_hyperbolic_functions
  Scenario Outline: Trigonometric and hyperbolic functions
    #* Calculate "<expression>"
    * ClickAndType "<expression>"
    * Click "=" button
    Then result is "<result>"

  Examples:
    | expression        | result       |
    | cos(0)            | 1            |
    | cos(<π>/5.4)      | 0.835487811  |
    | cos(<π>)          | -1           |
    | sin(0)            | 0            |
    | sin(3*<π>/2.5) | -0.587785252 |
    | sin(<π>/2)     | 1            |
    | tan(0)            | 0            |
    | tan(<π>/3)     | 1.732050808  |
    | cosh(0)           | 1            |
    | sinh(0)           | 0            |
    | tanh(0)           | 0            |
    | tanh(100)         | 1            |


  @trigonometric_function_undefined
  #@trigonometric_and_hyperbolic_functions
  Scenario: Trigonometric function - undefined
    #* Calculate "tan (π/2)"
    * ClickAndType "tan (<π>/2)"
    * Click "=" button
    Then "Tangent is undefined for angles that are multiples of π (180°) from π∕2 (90°)" error is displayed


  @trigonometric_and_hyperbolic_functions_inverted
  #@trigonometric_and_hyperbolic_functions
  Scenario Outline: Trigonometric and hyperbolic functions - inverted
    * Click "<function>" button
    * Click "Inverse" button
    * Calculate "<value>"
    Then result is "<result>"

  Examples:
    | function | value | result      |
    | cos      | 1     | 0           |
    | cos      | -1    | 3.141592654 |
    | sin      | 0     | 0           |
    | sin      | 1     | 1.570796327 |
    | tan      | 0     | 0           |
    | cosh     | 1     | 0           |
    | sinh     | 0     | 0           |
    | tanh     | 0     | 0           |


  @hyperbolic_functions_inverted_undefined
  #@trigonometric_and_hyperbolic_functions
  Scenario: Hyperbolic function - inverted - undefined
    * Click "tanh" button
    * Click "Inverse" button
    * Calculate "1"
    Then "Inverse hyperbolic tangent is undefined for values outside [-1, 1]" error is displayed


  @complex_number_functions
  @complex_numbers
  Scenario Outline: Complex number functions
    * Click "<function>" button
    * Calculate "<argument>"
    Then result is "<result>"

  Examples:
    | function | argument | result |
    | conj     | (2+8i)   | 2-8i   |
    | conj     | -2       | -2     |
    | Im       | (-2-8i)  | -8     |
    | Im       | -2       | 0      |
    | Re       | (-2-8i)  | -2     |
    | Re       | (-8i)    | 0      |


  @complex_numbers_calculation
  @complex_numbers
  Scenario Outline: Complex numbers calculation
    * Calculate "<expression>"
    Then result is "<result>"

  Examples:
    | expression      | result       |
    | (2+8i) * (3-2i) | 22+20i       |
    | (2+8i) + (3-2i) | 5+6i         |
    | (2+8i) - (3-2i) | -1+10i       |
    | (4+8i) / (2-2i) | -1+3i        |


  @pi_and_euler_number
  Scenario Outline: Pi and Euler's number
    * Click "<function>" button
    * Click "<argument>" button
    * Click "=" button
    Then result is "<result>"

  Examples:
    | function | argument | result |
    | cos      | π        | -1     |
    | sin      | π        | 0      |
    | ln       | e        | 1      |


  @absolute_value_calculation
  Scenario Outline: Absolute value
    * Type text "|"
    * Type text "<expression>"
    * Type text "|"
    * Click "=" button
    Then result is "<result>"

  Examples:
    | expression | result |
    | -123       | 123    |
    | 123        | 123    |
    | 0          | 0      |
    | -0         | 0      |


  @memory
  Scenario: Memory
    * Calculate "650*3"
    * Store into memory under "x" variable
    * Press "<Esc>"
    * Calculate "(x-1000)+x"
    * Store into memory under "y" variable
    * Press "<Esc>"
    * Click var button of the variable "x" in memory dialog
    * Click "+" button
    * Click var button of the variable "y" in memory dialog
    * Click "=" button
    * Store into memory under "x" variable
    * Press "<Esc>"
    * Calculate "x"
    Then result is "4850"
    * Press "<Esc>"
    * Click delete button of the variable "x" in memory dialog
    * Calculate "x"
    Then "Unknown variable “x”" error is displayed


  @factorial
  Scenario Outline: Factorial
    * Calculate "<expression>"
    Then result is "<result>"

  Examples:
    | expression | result                                                            |
    | 0!         | 1                                                                 |
    | 1!         | 1                                                                 |
    | 0.5!       | 0.886226925                                                       |
    | (-0)!      | 1                                                                 |
    | 10!        | 3628800                                                           |
    | 50!        | 30414093201713378043612608166064768844377641568960512000000000000 |


  @factorial_undefined
  @factorial
  Scenario: Factorial - undefined
    * Calculate "(-1)!"
    Then "Factorial is only defined for non-negative real numbers" error is displayed


  @gbz748731
  @rhbz836277
  @continued_computation
  Scenario: Continued computation
    * Calculate "25+5"
    * Calculate "/5"
    Then result is "6"
    * Press "<Esc>"
    * Calculate "10/5"
    * Click "cos" button
    * Click "π" button
    * Click "=" button
    Then result is "-2"


  @logarithm
  @logarithm_buttons
  Scenario Outline: Logarithms via buttons
    * Click "<function>" button
    * Type text "<argument>"
    * Click "=" button
    Then result is "<result>"

  Examples:
    | function | argument | result |
    | ln       | (e*e)    | 2      |
    | log      | 100      | 2      |
    | ln       | (e^2)    | 2      |
    | log      | (10^2)   | 2      |


  @logarithm
  @logarithm_typing
  Scenario Outline: Logarithms via buttons
    * Calculate "<expression>"
    Then result is "<result>"

  Examples:
    | expression | result |
    | ln(e*e)    | 2      |
    | log(100)   | 2      |
    | ln(e^2)    | 2      |
    | log(10^2)  | 2      |


  @logarithm
  @logarithm_undefined
  Scenario: Logarithm of undefined value
    * Calculate "log(0)"
    Then "Logarithm of zero is undefined" error is displayed


  @rhbz836971
  @gbz748729
  @logarithm_base_shortcut
  Scenario: Logarithm base set with shortcut
    * Click "log" button
    * With pressed "Alt" type "2"
    * Type text "512"
    * Click "=" button
    Then result is "9"


  @conversion_between_units
  Scenario Outline: Conversion between units
    * Set source conversion combobox to "<source_combo>"
    * Set target conversion combobox to "<target_combo>"
    * Calculate "<value>"
    Then the conversion right side is "<result>"

  Examples:
    | source_combo | target_combo | value  | result       |
    | Degrees      | Radians      | 45     | 0.79 radians |
    | Kilometers   | Miles        | 123    | 76.43 mi     |
    | Light Years  | Meters       | 10     | 9.46×10¹⁶ m  |
    | Hectares     | Acres        | 20     | 49.42 acres  |
    | US Gallons   | Liters       | 50     | 189.27 L     |
    | Kilograms    | Pounds       | 1500   | 3,306.93 lb  |
    | Days         | Years        | 368    | 1.01 years   |
    | Celsius      | Fahrenheit   | -18.92 | −2.06 ˚F     |


  @sub_and_superscript
  @superscript_type
  Scenario: Superscript by typing
    * Type text "3"
    * Press "<Control>3"
    * Click "=" button
    Then result is "27"


  @sub_and_superscript
  @superscript_button
  Scenario: Superscript entered by buttons
    * Click "3" button
    * Click "Superscript" button
    * Click "3" button
    * Click "=" button
    Then result is "27"


  @sub_and_superscript
  @subscript_type
  Scenario: Subscript by typing
    * Type text "log"
    * Press "<Alt>3"
    * Type text "27"
    * Click "=" button
    Then result is "3"


  @sub_and_superscript
  @subscript_button
  Scenario: Subscript entered by buttons
    * Click "log" button
    * Click "Subscript" button
    * Click "3" button
    * Click "Subscript" button
    * Click "2" button
    * Click "7" button
    * Click "=" button
    Then result is "3"


  @general_power_and_root
  @general_power
  Scenario Outline: General power
    * Calculate "<expression>"
    Then result is "<result>"

  Examples:
    | expression  | result     |
    | 1^100       | 1          |
    | 2^32        | 4294967296 |
    | (-3)^11     | -177147    |
    | (-3)^10     | 59049      |
    | (-2)^5      | -32        |
    | (3i)^3      | -27i       |


  @rhbz1002486
  @general_power_and_root
  @general_power_big
  Scenario: General power - big number
    * Calculate "6^(6^(6^6))"
    Then "Overflow error" error is displayed


  @general_power_and_root
  @general_root
  Scenario Outline: General power
    * With pressed "Alt" type "<first_exp>"
    * Press "<Ctrl>r"
    * Type text "<second_exp>"
    * Press "<Enter>"
    Then result is "<result>"

  Examples:
    | first_exp  | second_exp | result |
    | 100        | 1          | 1      |
    | 32         | 4294967296 | 2      |
    | 11         | (-177147)  | -3     |
    | 10         | 59049      | 3      |


  @factorize
  Scenario Outline: Factorize
    * Type text "<value>"
    * Press "<Control>f"
    Then result is "<result>"

  Examples:
    | value      | result            |
    | 12         | 2×2×3             |
    | 0          | 0                 |
    | -80        | -2×2×2×2×5        |
    | 123456     | 2×2×2×2×2×2×3×643 |
    | 2147483647 | 2147483647        |


  @additional_functions
  Scenario Outline: Additional functions
    * Calculate "<expression>"
    Then result is "<result>"

  Examples:
    | expression   | result |
    | int(-10.5)   | -10    |
    | frac(-10.5)  | -0.5   |
    | round(-10.5) | -11    |
    | floor(-10.5) | -11    |
    | ceil(-10.5)  | -10    |


  @rhbz836265
  @reserved_variable_names
  Scenario: Reserved variable names
    * Calculate "2*5"
    * Store into memory under "rand" variable
    * Press "<Esc>"
    * Calculate "rand*2"
    Then result is "20"
