@general_actions_feature
Feature: General actions

  Background:
    * Start application "gnome-calculator" via command in "session"
    * Wait until "Menu" "toggle button" appears


  @help_via_shortcut
  @help
  Scenario: Open Help via F1
    * Press "<F1>"
    * Wait 5 seconds
    Then Item "Calculator Help" "frame" is "showing" in "yelp"

  @help_via_application_menu
  @help
  Scenario: Open Help via application menu
    * Click "Help" in application menu
    * Wait 5 seconds
    Then Item "Calculator Help" "frame" is "showing" in "yelp"

  @about_dialog
  Scenario: About dialog
    * Click "About Calculator" in application menu
    Then dialog "About Calculator" is shown

  @clear_display
  Scenario: Clear display
    * Switch to Basic mode
    * Type text "123 + 123 +"
    * Press "<Esc>"
    * Type text "10"
    * Press "<Enter>"
    Then result is "10"

  @copy_paste
  Scenario: Copy and Paste
    * Switch to Basic mode
    * Type text "123"
    * Press "<Control>a"
    * Press "<Control>c"
    Then result is "123"
    * Press "<Control>v"
    * Press "<Control>v"
    Then result is "123123"

  @undo_redo
  Scenario: Undo and Redo actions
    * Switch to Basic mode
    * Type text "123"
    * Press "<Control>z"
    Then result is "12"
    * Press "<Control>z"
    Then result is "1"
    * Press "<Control><Shift>z"
    Then result is "12"
    * Press "<Control><Shift>z"
    Then result is "123"


  @change_calculator_modes
  @change_calculator_modes_basic
  Scenario: Change calculator modes - basic
    * Switch to Basic mode
    Then current mode is Basic mode


  @change_calculator_modes
  @change_calculator_modes_advanced
  Scenario: Change calculator modes - advanced
    * Switch to Advanced mode
    Then current mode is Advanced mode


  @change_calculator_modes
  @change_calculator_modes_financial
  Scenario: Change calculator modes - financial
    * Switch to Financial mode
    Then current mode is Financial mode


  @change_calculator_modes
  @change_calculator_modes_programming
  Scenario: Change calculator modes - programming
    * Switch to Programming mode
    Then current mode is Programming mode


  @rhbz1070259
  @preferences
  Scenario: Preferences
    * Set the "accuracy" to "9" value
    * Switch to Advanced mode
    * Calculate "123456.654321*10^6"
    * Set "Number Format:" to "Fixed" in Preferences
    * Make sure the toggle button "Thousands separators" in Preferences is unchecked
    Then result is "123456654321"
    * Make sure the toggle button "Thousands separators" in Preferences is checked
    Then result is "123,456,654,321"
    * Set "Number Format:" to "Scientific" in Preferences
    Then result is "1.234566543×10¹¹"
    * Set "Number Format:" to "Engineering" in Preferences
    Then result is "123.456654321×10⁹"
    * Set "Number of decimals" to "5" in Preferences
    Then result is "123.45665×10⁹"
