@programming_mode_feature
Feature: Programming mode

  Background:
    * Set the "number-format" to "fixed" value
    * Set the "accuracy" to "9" value
    * Set the "show-thousands" to "false" value
    * Start application "gnome-calculator" via command "gnome-calculator -m programming" in "session"
    * Wait until "Menu" "toggle button" appears


  @programming_calculation
  Scenario Outline: Arithmentics
    * Set numeric base to "Decimal"
    * Calculate "<expression>"
    Then result is "<result>"

  Examples:
    | expression            | result     |
    | 123456789 + 987654321 | 1111111110 |
    | 987654321 + 0         | 987654321  |
    | 987654321 - 987654322 | -1         |
    | -98765-0              | -98765     |
    | 3/6                   | 0.5        |
    | -8/2                  | -4         |
    | 10 / 3 * 3            | 10         |
    | 6 / (3 * 2)           | 1          |


  @boolean_algebra
  @boolean_algebra_and
  Scenario: Boolean algebra - AND
    * Set "Word size:" to "8 bits" in Preferences
    * Set numeric base to "Binary"
    * Type text "1110101"
    * Click "AND" button
    * Type text "1010010"
    * Click "=" button
    Then result is "1010000"


  @boolean_algebra
  @boolean_algebra_or
  Scenario: Boolean algebra - OR
    * Set "Word size:" to "8 bits" in Preferences
    * Set numeric base to "Binary"
    * Type text "1010000"
    * Click "OR" button
    * Type text "1001"
    * Click "=" button
    Then result is "1011001"


  @boolean_algebra
  @boolean_algebra_not
  Scenario: Boolean algebra - NOT
    * Set "Word size:" to "8 bits" in Preferences
    * Set numeric base to "Binary"
    * Click "NOT" button
    * Type text "1001110"
    * Click "=" button
    Then result is "10110001"


  @boolean_algebra
  @boolean_algebra_xor
  Scenario: Boolean algebra - XOR
    * Set "Word size:" to "8 bits" in Preferences
    * Set numeric base to "Binary"
    * Type text "11110010"
    * Click "XOR" button
    * Type text "10111111"
    * Click "=" button
    Then result is "1001101"


  @modulus_division
  Scenario Outline: Modulus division
    * Set numeric base to "Decimal"
    * Type text "<first>"
    * Click "mod" button
    * Type text "<second>"
    * Click "=" button
    Then result is "<result>"

  Examples:
    | first | second | result |
    | 12    | 5      | 2      |
    | 21    | -9     | -6     |
    | -12   | 5      | 3      |


  @conversions_between_numeric_bases
  @conversions_between_numeric_bases_octal
  Scenario: Conversions between numeric bases - octal
    * Set "Word size:" to "8 bits" in Preferences
    * Set numeric base to "Binary"
    * Calculate "1001010"
    Then result is "1001010"
    * Set numeric base to "Octal"
    Then result is "112"


  @conversions_between_numeric_bases
  @conversions_between_numeric_bases_decimal
  Scenario: Conversions between numeric bases - decimal
    * Set "Word size:" to "8 bits" in Preferences
    * Set numeric base to "Binary"
    * Calculate "1001010"
    Then result is "1001010"
    * Set numeric base to "Decimal"
    Then result is "74"


  @conversions_between_numeric_bases
  @conversions_between_numeric_bases_hexadecimal
  Scenario: Conversions between numeric bases - hexadecimal
    * Set "Word size:" to "8 bits" in Preferences
    * Set numeric base to "Binary"
    * Calculate "1001010"
    Then result is "1001010"
    * Set numeric base to "Hexadecimal"
    Then result is "4A"


  @complements
  Scenario Outline: Complements
    * Set "Word size:" to "8 bits" in Preferences
    * Set numeric base to "Binary"
    * Click "<function>" button
    * Type text "<value>"
    * Click "=" button
    Then result is "<result>"

  Examples:
    | function | value    | result    |
    | ones     | 11111111 | 0         |
    | ones     | 00000000 | 11111111  |
    | ones     | 10101010 | 1010101   |
    | twos     | 11111111 | 1         |
    | twos     | 00000000 | 100000000 |
    | twos     | 10101010 | 1010110   |


  @shifts
  Scenario: Shifts
    * Set "Word size:" to "8 bits" in Preferences
    * Set numeric base to "Binary"
    * Type text "1010"
    * Select "10 places" from "Shift Left" menu
    * Click "=" button
    Then result is "10100000000000"
    * Select "12 places" from "Shift Right" menu
    * Click "=" button
    Then result is "10"
    * Select "15 places" from "Shift Right" menu
    * Click "=" button
    Then result is "0"


  @rhbz1046143
  @exponentiation_different_bases
  @exponentiation_different_bases_binary
  Scenario: Exponentiation in different numeric base - binary
    * Set numeric base to "Binary"
    * Type text "10^-1"
    * Click "=" button
    Then result is "0.1"
    * Type text "^-1"
    * Click "=" button
    Then result is "10"


  @rhbz1046143
  @exponentiation_different_bases
  @exponentiation_different_bases_octal
  Scenario: Exponentiation in different numeric base - octal
    * Set numeric base to "Octal"
    * Type text "77^-1"
    * Click "=" button
    Then result is "0.01010101"
    * Type text "^-1"
    * Click "=" button
    Then result is "77"


  @rhbz1046143
  @exponentiation_different_bases
  @exponentiation_different_bases_decimal
  Scenario: Exponentiation in different numeric base - decimal
    * Set numeric base to "Decimal"
    * Type text "99^-1"
    * Click "=" button
    Then result is "0.01010101"
    * Type text "^-1"
    * Click "=" button
    Then result is "99"


  @rhbz1046143
  @exponentiation_different_bases
  @exponentiation_different_bases_hexadecimal
  Scenario: Exponentiation in different numeric base - hexadecimal
    * Set numeric base to "Hexadecimal"
    * Type text "EF^-1"
    * Click "=" button
    Then result is "0.0112358E7"
    * Type text "^-1"
    * Click "=" button
    Then result is "EF"
