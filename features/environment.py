#!/usr/bin/python3
# -*- coding: UTF-8 -*-

import sys
import traceback
#from common_steps import App, common_before_all, common_after_step, common_after_scenario
from qecore.sandbox import TestSandbox


def before_all(context):
    """Setup stuff
    Being executed before all features
    """

    try:
        context.sandbox = TestSandbox("gnome-calculator")
        context.sandbox.record_video = False
        context.calculator = context.sandbox.get_app("gnome-calculator")
    except Exception as error:
        print(f"Environment error: before_all: {error}")
        traceback.print_exc(file=sys.stdout)
        sys.exit(1)


def before_scenario(context, scenario):
    context.first_click = True
    try:
        context.sandbox.before_scenario(context, scenario)
    except Exception as error:
        print(f"Environment error: before_scenario: {error}")
        traceback.print_exc(file=sys.stdout)
        sys.exit(1)


def after_scenario(context, scenario):
    try:
        context.sandbox.after_scenario(context, scenario)
    except Exception as error:
        print(f"Environment error: after_scenario: {error}")
        traceback.print_exc(file=sys.stdout)
