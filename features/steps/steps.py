#!/usr/bin/python3
# -*- coding: UTF-8 -*-
from behave import step

from dogtail.rawinput import typeText, keyNameToKeyCode, keyNameAliases, pressKey, click
from dogtail.utils import doDelay, GnomeShell
#from common_steps import limit_execution_time_to
#from common_steps.app import *
#from common_steps.appmenu import *
from qecore.common_steps import *

from pyatspi import Registry as registry
from pyatspi import KEY_PRESS, KEY_RELEASE
from gi.repository import Gdk
from time import sleep
import math
import os


@step('Wait {sleep_number} seconds')
def wait_for_app_starts(context, sleep_number):
    sleep(int(sleep_number))


@step('current mode is {mode:w} mode')
def check_recent_mode_dummy(context, mode):
    real = context.calculator.instance.findChild(lambda x: x.name == 'Re', requireResult=False, retry=False)
    real = real.showing if real else False

    ctrm = context.calculator.instance.findChild(lambda x: x.name == 'Ctrm', requireResult=False, retry=False)
    ctrm = ctrm.showing if ctrm else False

    or_btn = context.calculator.instance.findChild(lambda x: x.name == 'OR', requireResult=False, retry=False)
    or_btn = or_btn.showing if or_btn else False

    if mode == 'Basic':
        assert not real, 'Some buttons from Advanced mode are showing!'
        assert not or_btn, 'Some buttons from Programming mode are showing!'
        assert not ctrm, 'Some buttons from Financial mode are showing!'
    elif mode == 'Advanced':
        assert real, 'Some buttons from Advanced mode are not showing!'
        assert not or_btn, 'Some buttons from Programming mode are showing!'
        assert not ctrm, 'Some buttons from Financial mode are showing!'
    elif mode == 'Programming':
        assert not real, 'Some buttons from Advanced mode are showing!'
        assert or_btn, 'Some buttons from Programming mode are not showing!'
        assert not ctrm, 'Some buttons from Financial mode are showing!'
    elif mode == 'Financial':
        assert not real, 'Some buttons from Advanced mode are showing!'
        assert not or_btn, 'Some buttons from Programming mode are showing!'
        assert ctrm, 'Some buttons from Financial mode are not showing!'
    else:
        assert False, 'Unrecognized mode!'


@step('Switch to {mode:w} mode')
def switch_to_mode(context, mode):
    btns = context.calculator.instance.findChildren(lambda x: x.name == 'Menu')
    context.execute_steps('* Wait until "Menu" "toggle button" is "showing"')
    # it happens that calc's window is opened inactive
    context.calculator.instance.child('', roleName='viewport').click()
    btns[1].click()
    sleep(1)
    c = 0
    while (not btns[1].checked) and c < 10:
        btns[1].click()
        sleep(1)
        c += 1
    context.execute_steps(f'* Wait until "{mode} Mode" "radio button" is "showing"')
    context.execute_steps(f'* Left click "{mode} Mode" "radio button" that is "showing"')
    showing = False
    c = 0
    while (not showing) and c < 10:
        try:
            check_recent_mode_dummy(context, mode)
        except AssertionError:
            sleep(0.5)
            c += 1
            continue
        showing = True


#@limit_execution_time_to(seconds=30)
#def wait_until_spinner_showing(context):
    # Wait until spinner disappears
#    sleep(0.1)
#    spinner = context.calculator.instance.child('Spinner')
#    while spinner.showing:
#        sleep(0.1)


@step('Calculate "{expression}"')
def calculate(context, expression):
    context.calculator.instance.child(roleName='editbar').grabFocus()
    typeText(expression)
    context.calculator.instance.findChildren(lambda x: x.name == '=' and x.showing and x.roleName == 'push button')[0].click()
#    wait_until_spinner_showing(context)


@step('result is {comp} than "{result}"')
@step('result is "{result}"')
def verify_result(context, result, comp=None):
    # Replace unicode negative sign to simple '-'
    actual = context.calculator.instance.child(roleName='editbar').text.replace('−', '-')

    if comp:
        if comp.lower() == 'greater':
            assert float(actual) > float(result), "Incorrect result, '%s' was expected to be greater then '%s'" % \
                (actual, result)
        else:
            assert float(actual) < float(result), "Incorrect result, '%s' was expected to be lower then '%s'" % \
                (actual, result)
    else:
        assert result == actual, "Incorrect result, expected '%s' but was '%s'" % (result, actual)


@then('"{message}" error is displayed')
def error_message_displayed(context, message):
    actual = context.calculator.instance.child(roleName='text').parent.textentry('').text
    assert message == actual, "Incorrect error, expected '%s' but was '%s'" % (message, actual)


@step('Type text "{text}"')
def type_text(context, text):
    typeText(text)


@step('Click "{buttonname}" button')
def click_button(context, buttonname):
    cur_text = context.calculator.instance.child(roleName='editbar').text
    buttons = context.calculator.instance.findChildren(lambda x: x.name == buttonname and
                                                          x.roleName in ['push button', 'toggle button'] and x.showing)
    assert len(buttons) == 1, "Either none or more than one button found."
    buttons[0].click()
    if context.first_click and len(cur_text) == 0:
        if len(context.calculator.instance.child(roleName='editbar').text) == 0:
            buttons[0].click()
    context.first_click = False


@step('Select "{menu_item}" from "{menu_name}" menu')
def select_item_from_menu(context, menu_item, menu_name):
    context.execute_steps('* Click "%s" button' % menu_name)
    context.calculator.instance.child(name=menu_item, roleName='push button').click()


@step('dialog "{name}" is shown')
@step('dialog "{name}" is {invisible:w} shown')
def check_dialog_visibility(context, name, invisible=None):
    dialog_names = [x.name for x in context.calculator.instance.children if x.roleName == 'dialog']
    if invisible:
        assert name not in dialog_names, 'Dialog "%s" is shown' % name
    else:
        assert name in dialog_names, 'Dialog "%s" is not shown' % name


@step('Set "{option}" to "{value}" in "{dialog_name}" dialog')
def set_option_to_value_in_dialog(context, option, value, dialog_name):
    dialog = context.calculator.instance.child(name=dialog_name, roleName='dialog')
    option_ac = dialog.child(option)
    value_ac = option_ac.labelee
    if value_ac is None or value_ac.roleName in ['spin button', 'text']:
        if value_ac is None:
            # workaround due rhbz1286075
            value_ac = dialog.findChildren(lambda x: x.roleName == 'text' and x.labeler is None)[-1]
        value_ac.click()
        keyCombo("<Control>a")
        keyCombo("<Delete>")
        typeText(value)
    elif value_ac.roleName == 'combo box':
        value_ac.click()
        value_ac.child(value).click()


@step('Set "{option}" to "{value}" in Preferences')
def set_option_to_value_in_preferences(context, option, value):
    context.execute_steps('* Click "Preferences" in application menu')
    context.execute_steps('Then dialog "Preferences" is shown')
    context.execute_steps('* Set "%s" to "%s" in "Preferences" dialog' % (option, value))
    context.calculator.instance.child('Preferences', roleName='dialog').child('Close').click()


@step('Make sure the toggle button "{toggle}" in Preferences is {state:w}')
def make_sure_the_toggle_button_has_state(context, toggle, state):
    context.execute_steps('* Click "Preferences" in application menu')
    context.execute_steps('Then dialog "Preferences" is shown')
    pref = context.calculator.instance.child(name='Preferences', roleName='dialog')
    if toggle == 'Thousands separators':
        button = pref.findChildren(lambda x: x.roleName=='toggle button')[0]
    elif toggle == 'Trailing zeros':
        button = pref.findChildren(lambda x: x.roleName=='toggle button')[0]
    else:
        assert False, 'Unknown button name! Only "Thousands separators" and "Trailing zeros" exists.'
    if state.lower() == 'checked':
        if not button.isChecked:
            button.click()
    elif state.lower() == 'unchecked':
        if button.isChecked:
            button.click()
    else:
        assert False, 'Not supported state! Only "checked" and "unchecked" are available.'
    keyCombo("<Esc>")


@step('Set the "{option}" to "{value}" value')
def set_option_to_value_dconf(context, option, value):
    os.system('gsettings set org.gnome.calculator %s %s' % (option, value))


@step('Click "{button_name}" in "{dialog_name}" dialog')
def click_button_in_dialog(context, button_name, dialog_name):
    dialog = context.calculator.instance.child(name=dialog_name, roleName='dialog')
    buttons = dialog.findChildren(lambda x: x.name == button_name and
                                            x.roleName in ['push button', 'toggle button'] and x.showing)
    assert len(buttons) == 1, "Either none or more than one button found."
    buttons[0].click()


@step('Store into memory under "{var:w}" variable')
def store_into_memory_as_variable(context, var):
    context.execute_steps('* Click "Memory" button')
    save_button = context.calculator.instance.findChild(lambda x: x.description == 'Store value into existing or new variable')
    save_button.parent.child(roleName='text').typeText(var)
    save_button.click()
    context.execute_steps('* Press "<Esc>"')


@step('Click {action:w} button of the variable "{var:w}" in memory dialog')
def do_action_in_memory_dialog(context, action, var):
    context.execute_steps('* Click "Memory" button')
    sb = context.calculator.instance.findChild(lambda x: x.description == 'Store value into existing or new variable')
    lb = sb.parent.parent.child(roleName='list box')
    var_buttons = lb.findChildren(lambda x: x.roleName == 'label' and x.name.split('=')[0].strip() == var)
    assert len(var_buttons) == 1, 'Only one button should be present! Found %d buttons' % len(var_buttons)
    var_button = var_buttons[0]
    # vars are now in scroll bar. If the var is not showing we have to scroll down to it
    if not var_button.showing:
        lb.click(2)
        while not var_button.showing:
            pressKey('Down')
    # right now there is a button for input variable to expr and to delete variable
    if action.lower() == 'var':
        var_button.click()
    elif action.lower() == 'delete':
        var_button.parent.children[1].click()
    context.execute_steps('* Press "<Esc>"')


@step('Set numeric base to "{new_base}"')
def set_numeric_base(context, new_base):
    base = context.calculator.instance.findChildren(lambda x: x.roleName == 'combo box' and x.showing)[0]
    base.click()
    base.child(new_base).click()


@step('Set {order:w} conversion combobox to "{value}"')
def set_conversion_combobox_to(context, order, value):
    comboboxes = context.calculator.instance.findChildren(lambda x: x.roleName == 'combo box' and x.showing)
    if order == 'source':
        combo = comboboxes[1]
    elif order == 'target':
        combo = comboboxes[0]
    else:
        assert False, 'Unrecognized combobox, only \'source\' and \'target\' are possible. \'%s\' recieved.' % order
    if combo.combovalue == value:
        return

    combo.click()
    wanted = combo.child(value)

    while not wanted.showing:
        pressKey("Down")
        sleep(1)
    pressKey("Right")
    while not wanted.isSelected:
        pressKey("Down")

    pressKey("Enter")


    assert combo.combovalue == value, combo.combovalue


@then('the conversion {side:w} side is "{result}"')
@then('the conversion is "{result}"')
def check_conversion_equation(context, side='', result=''):
    equation_label = context.calculator.instance.child(roleName='combo box').parent.children[0]
    sides = list(map(lambda x: x.name, equation_label.children))
    if side == 'left':
        assert sides[0] == result,\
            'The left side of equation is not the same as expected. The expected was "%s", recieved was "%s".' %\
            (result, sides[0])
    elif side == 'right':
        assert sides[2] == result,\
            'The right side of equation is not the same as expected. The expected was "%s", recieved was "%s".' %\
            (result, sides[2])
    else:
        assert ' '.join(sides) == result,\
            'The conversion equation is not the same as expected. The expected was "%s", recieved was "%s".' %\
            (result, equation_label)


@then('the conversion {side:w} side is in "{unit}" currency')
@then('the conversion {side:w} side is in "{unit}" unit')
def check_conversion_unit(context, side, unit):
    equation_label = context.calculator.instance.child(roleName='combo box').parent.children[0]
    sides = list(map(lambda x: x.name, equation_label.children))
    if side == 'left':
        assert unit in sides[0],\
            'The left side of equation is not in the expcted unit. The expected was "%s", the whole left side is "%s".' %\
            (unit, sides[0])
    elif side == 'right':
        assert unit in sides[2],\
            'The right side of equation is not in the expcted unit. The expected was "%s", the whole right side is "%s".' %\
            (unit, sides[2])


@step('Press switch conversion button')
def switch_conversion(context):
    context.calculator.instance.findChild(lambda x: x.description == 'Switch conversion units' and x.showing).click()


@step('Pi')
def pi(context):
    keyCombo("<Ctrl>p")


@step('With pressed "{key}" type "{text}"')
def type_text_while_pressed_key(context, key, text):
    code = keyNameToKeyCode(keyNameAliases[key.lower()])
    registry.generateKeyboardEvent(code, None, KEY_PRESS)
    typeText(text)
    registry.generateKeyboardEvent(code, None, KEY_RELEASE)


@step('ClickAndType "{expr}"')
def click_and_type(context, expr):
    i = 0
    while i < len(expr):
        if expr[i] == '<':
            end = expr[i:].index('>')+i
            key = expr[i+1:end]
            context.execute_steps('* Click "{}" button'.format(key))
            i = end+1
        else:
            end = len(expr) if '<' not in expr[i:] else (expr[i:].index('<')+i)
            typeText(expr[i:end])
            i = end

@step('Click "{name}" in application menu')
def click_menu_about(context, name):
    btns = context.calculator.instance.findChildren(lambda x: x.name == 'Menu')
    btns[0].click()
    menuitem = context.calculator.instance.child(name=name)
    menuitem.click()
    doDelay(2)

# @step('Click X button on application panel')
# def click_x_button(context):
#     panel = context.calculator.instance[0][0]
#     offset = panel.size[1]/2
#     x = panel.position[0] + panel.size[0] - offset
#     y = panel.position[1] + offset
#     click(x, y)
