@financial_mode_feature
Feature: Financial mode

  Background:
    * Set the "number-format" to "fixed" value
    * Set the "accuracy" to "9" value
    * Set the "show-thousands" to "false" value
    * Start application "gnome-calculator" via command "gnome-calculator -m financial" in "session"
    * Wait until "Menu" "toggle button" appears


  @financial_calculation
  Scenario Outline: Arithmentics
    * Calculate "<expression>"
    Then result is "<result>"

  Examples:
    | expression            | result     |
    | 123456789 + 987654321 | 1111111110 |
    | 987654321 + 0         | 987654321  |
    | 987654321 - 987654322 | -1         |
    | -98765-0              | -98765     |
    | 3/6                   | 0.5        |
    | -8/2                  | -4         |
    | 10 / 3 * 3            | 10         |
    | 6 / (3 * 2)           | 1          |


  @rhbz1250909
  @financial_functions
  @financial_function_ctrm
  Scenario: Financtial function - Compounding Term
    * Click "Ctrm" button
    * Set "Periodic Interest Rate:" to "0.0075" in "Compounding Term" dialog
    * Set "Future Value:" to "18000" in "Compounding Term" dialog
    * Set "Present Value:" to "8000" in "Compounding Term" dialog
    * Click "Calculate" in "Compounding Term" dialog
    Then result is "108.528988997"


  @financial_functions
  @financial_function_ddb
  Scenario: Financtial function - Double Declining Depreciation
    * Click "Ddb" button
    * Set "Cost:" to "8000" in "Double-Declining Depreciation" dialog
    * Set "Life:" to "6" in "Double-Declining Depreciation" dialog
    * Set "Period:" to "4" in "Double-Declining Depreciation" dialog
    * Click "Calculate" in "Double-Declining Depreciation" dialog
    Then result is "790.12345679"


  @financial_functions
  @financial_function_fv
  Scenario: Financtial function - Future Value
    * Click "Fv" button
    * Set "Periodic Payment:" to "4000" in "Future Value" dialog
    * Set "Periodic Interest Rate:" to "0.08" in "Future Value" dialog
    * Set "Number of Periods:" to "20" in "Future Value" dialog
    * Click "Calculate" in "Future Value" dialog
    Then result is "183047.857192465"


  @gbz758733
  @rhbz1286075
  @financial_functions
  @financial_function_gpm
  Scenario: Financtial function - Gross Profit Margin
    * Click "Gpm" button
    * Set "Cost:" to "10" in "Gross Profit Margin" dialog
    * Set "Margin:" to "0.35" in "Gross Profit Margin" dialog
    * Click "Calculate" in "Gross Profit Margin" dialog
    Then result is "15.384615385"


  @financial_function
  @financial_function_pmt
  Scenario: Financtial function - Periodic Payment
    * Click "Pmt" button
    * Set "Principal:" to "120000" in "Periodic Payment" dialog
    * Set "Periodic Interest Rate:" to "0.00917" in "Periodic Payment" dialog
    * Set "Term:" to "360" in "Periodic Payment" dialog
    * Click "Calculate" in "Periodic Payment" dialog
    Then result is "1143.15079033"


  @financial_functions
  @financial_function_pv
  Scenario: Financtial function - Present Value
    * Click "Pv" button
    * Set "Periodic Payment:" to "50000" in "Present Value" dialog
    * Set "Periodic Interest Rate:" to "0.09" in "Present Value" dialog
    * Set "Number of Periods:" to "20" in "Present Value" dialog
    * Click "Calculate" in "Present Value" dialog
    Then result is "456427.283454296"


  @financial_functions
  @financial_function_rate
  Scenario: Financtial function - Periodic Interest Rate
    * Click "Rate" button
    * Set "Future Value:" to "30000" in "Periodic Interest Rate" dialog
    * Set "Present Value:" to "20000" in "Periodic Interest Rate" dialog
    * Set "Term:" to "60" in "Periodic Interest Rate" dialog
    * Click "Calculate" in "Periodic Interest Rate" dialog
    Then result is "0.006780637"


  @rhbz1250909
  @financial_functions
  @financial_function_sln
  Scenario: Financtial function - Straight Line Depreciation
    * Click "Sln" button
    * Set "Cost:" to "8000" in "Straight-Line Depreciation" dialog
    * Set "Salvage:" to "900" in "Straight-Line Depreciation" dialog
    * Set "Life:" to "4" in "Straight-Line Depreciation" dialog
    * Click "Calculate" in "Straight-Line Depreciation" dialog
    Then result is "1775"


  @rhbz1250909
  @financial_functions
  @financial_function_syd
  Scenario: Financtial function - Sum-of-the-Years'-Digits Depreciation
    * Click "Syd" button
    * Set "Cost:" to "8000" in "Sum-of-the-Years’-Digits Depreciation" dialog
    * Set "Salvage:" to "900" in "Sum-of-the-Years’-Digits Depreciation" dialog
    * Set "Life:" to "6" in "Sum-of-the-Years’-Digits Depreciation" dialog
    * Set "Period:" to "4" in "Sum-of-the-Years’-Digits Depreciation" dialog
    * Click "Calculate" in "Sum-of-the-Years’-Digits Depreciation" dialog
    Then result is "1014.285714286"


  @financial_functions
  @financial_function_term
  Scenario: Financtial function - Financial term
    * Click "Term" button
    * Set "Periodic Payment:" to "1800" in "Payment Period" dialog
    * Set "Future Value:" to "120000" in "Payment Period" dialog
    * Set "Periodic Interest Rate:" to "0.11" in "Payment Period" dialog
    * Click "Calculate" in "Payment Period" dialog
    Then result is "20.316818943"


  @switch_currency_units
  Scenario: Switch currency units
    * Set source conversion combobox to "Euro"
    * Set target conversion combobox to "US Dollar"
    * Calculate "123"
    Then the conversion left side is in "€" currency
    And the conversion right side is in "$" currency
    And result is "123"
    * Press switch conversion button
    Then the conversion left side is in "$" currency
    And the conversion right side is in "€" currency
