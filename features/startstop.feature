@startstop_feature
Feature: Start stop tests

  @start_via_command
  Scenario: Start via command
    * Start gnome-calculator via command
    Then gnome-calculator should start

  @start_via_menu
  Scenario: Start via menu
    * Start gnome-calculator via menu
    Then gnome-calculator should start

  @quit_via_shortcut
  Scenario: Alt-F4 to quit application
    * Start gnome-calculator via command
    * Press "<Alt><F4>"
    Then gnome-calculator shouldn't be running anymore

  @close_via_gnome_panel
  Scenario: Close via menu
    * Start gnome-calculator via menu
    * Click "Quit" in GApplication menu
    Then gnome-calculator shouldn't be running anymore

  @close_via_button
  Scenario: Close via X button
    * Start gnome-calculator via menu
    * Left click "Close" "push button"
    Then gnome-calculator shouldn't be running anymore


