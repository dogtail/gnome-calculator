#!/bin/bash
set -x
git submodule update --init --recursive
# Here we store exit code for the task in tmp file
# Because we need a report
# TODO: write a better rhts-run-simple-test


rm -rf /home/test/Videos/*

if [ -n "$DTTEST" ]; then
    export TEST=$DTTEST
fi

sudo -u test dogtail-run-headless-next --force-xorg "behave -t $1 -k -f html -o /tmp/report_$TEST.html -f plain"; rc=$?


RESULT="FAIL"
if [ $rc -eq 0 ]; then
  RESULT="PASS"
fi
rhts-report-result $TEST $RESULT "/tmp/report_$TEST.html"
exit $rc
